package libs;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;

import static libs.ConfigData.ui;

import java.util.concurrent.TimeUnit;

/**
 * Created by Orest on 03.11.2015.
 */
public class WebElementsOnPage {
    WebDriver driver;
    Logger log;
    WebDriverWait wait;
    JavascriptExecutor js = (JavascriptExecutor) driver;

    public WebElementsOnPage(WebDriver externalDriver) {
        this.driver = externalDriver;
        log = Logger.getLogger(getClass());
        wait = new WebDriverWait(driver, 5);
        
    }

    /**
     * Method open browser and URL
     */
    public void openBrowserAndUrl(String url){
        try {
//        	driver.manage().window().maximize(); changed steps
            driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS); //implicitly waiting for 5 seconds
            driver.manage().window().maximize();
            driver.get(url);
            log.info("Browser and url was opened");
        } catch (Exception e) {
            log.error("Can't open browser or Url " + e);
            Assert.assertTrue("Browser can't start ", false);
        }
    }

    /**
     * Method close browser
     */
    public void closeBrowser(){
        try {
            driver.quit();
            log.info("Browser was closed");
        } catch (Exception e) {
            log.error("Browser was NOT closed");
        }
    }

    /**
     * Method click on Button
     */
    public boolean clickOnButton(String buttonLocator){
        try {
            wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(ui(buttonLocator)))); //EXPLICITLY wait; use By.ui for all
            driver.findElement(ui(buttonLocator)).click();
            log.info("Button was clicked");
            return true;
        } catch (Exception e) {
            log.error("Catch " + e);
            return false;
        }
    }

    /**
     * Method click on Link
     */
    public boolean clickOnLink(String linkLocator) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(ui(linkLocator))));
            driver.findElement(ui(linkLocator)).click();
            log.info("Link was clicked");
            return true;
        } catch (Exception e) {
            log.error("Catch " + e);
            return false;
        }
    }

    /**
     * Method input text to input field
     * TODO: this method require check - maybe for  visibilityOfElementLocated we can use tempInputElement which is initialized later?
     * TODO: updated - tabs on page get attribute "Selected", can make a check on this
     */
    public boolean inputTextIntoInputField(String inputLocator, String text){
        try {
        	WebElement tempInputElement;
        	tempInputElement = driver.findElement(ui(inputLocator));
            wait.until(ExpectedConditions.visibilityOfElementLocated(ui(inputLocator))); //TODO: find best "wait" for input taking in account there are Tabs on editors and some input may not be visible
            log.info("UI mapping value at current moment - " + inputLocator);           
//            tempInputElement = driver.findElement(ui(inputLocator));
//            tempInputElement.sendKeys(Keys.DELETE);
            tempInputElement.clear();
            log.info("Field was cleared");
            tempInputElement.sendKeys(text);
            log.info("Text " + text + " was entered into input using " + inputLocator);
            return true;
        } catch (Exception e) {
            log.error("locator " + inputLocator + " used. Catch " + e);
            return false;
        }
    }

    /**
     * Method Click on check box
     */
    public boolean clickOnCheckbox(String checkboxLocator) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(ui(checkboxLocator))));
            WebElement checkbox = driver.findElement(ui(checkboxLocator));
            //checking if check box is selected by default
            if (checkbox.isSelected()) {
                log.info("Checkbox has been already selected");
                return true;
            }
            // if not selected clicking to select it
            else {
                checkbox.click();
                log.info("Checkbox has been clicked to be selected");
                return true;
            }
        } catch (Exception e) {
            log.error("Catch " + e);
            return false;
        }
    }

    /**
     * Improved method set state to check box
     * stateFromCase (Value MUST BE "YES" or "NO")
     *
     */
    public boolean setStateOnCheckBox(String checkboxLocator, String stateFromCase){
        try{
            wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(ui(checkboxLocator))));
            WebElement checkboxTemp = driver.findElement(ui(checkboxLocator));
            if(checkboxTemp.isSelected() && stateFromCase.equals("YES")){
                log.info("Checkbox already checked and state is " + stateFromCase);
                return true;
            }else
            if(checkboxTemp.isSelected() && stateFromCase.equals("NO")){
                checkboxTemp.click();
                log.info("Checkbox was clicked and state is " + stateFromCase);
                return true;
            } else
            if(!checkboxTemp.isSelected() && stateFromCase.equals("YES")){
                checkboxTemp.click();
                log.info("Checkbox was clicked and state " + stateFromCase );
                return true;
            }else
            if (!checkboxTemp.isSelected() && stateFromCase.equals("NO")){
                log.info("Checkbox already unchecked and state is " + stateFromCase);
                return true;
            }else{
                log.error("This almost never can happen so need further investigation: method setStateOnCheckBox");
                return false;
            }
        } catch (Exception e) {
            log.error("Catch " + e);
            return false;
        }
    }

    /**
     * Method click radioButton by locator
     */
    public boolean radioButtonSet(String radioButtonLocator){
        try{
            wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(ui(radioButtonLocator))));
            driver.findElement(ui(radioButtonLocator)).click();
            return true;
        }catch (Exception e) {
            log.error("Catch " + e);
            return false;
        }
    }

    /**
     * Select drop down value WITHOUT click
     * Method contains check if Select is clickable
     * @param dropDownLocator
     * @param valueFromCase
     * @return
     */
    public boolean selectFromDropDownByValueFromCase(String dropDownLocator, String valueFromCase){
        try{
            wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(ui(dropDownLocator))));
            WebElement parentDropDownElement;
            parentDropDownElement = driver.findElement(ui(dropDownLocator));
            Select selectFromParentDropDownElement;
            selectFromParentDropDownElement = new Select(parentDropDownElement);
            selectFromParentDropDownElement.selectByValue(valueFromCase);
            log.info("In dropdown was selected value " + valueFromCase);
            return true;
        }catch (Exception e) {
            log.error("Catch " + e);
            return false;
        }
    }

    /**
     * Method to find and click on VISIBLE text value in drop down
     * @param dropDownLocator
     * @param valueFromCase
     * @return
     */
    public boolean selectFromDropdownByVisibleText(String dropDownLocator, String valueFromCase){
        try{
            wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(ui(dropDownLocator))));
            WebElement parentDropDownElement;
            parentDropDownElement = driver.findElement(ui(dropDownLocator));
            Select selectFromParentDropDownElement;
            selectFromParentDropDownElement = new Select(parentDropDownElement);
            selectFromParentDropDownElement.selectByVisibleText(valueFromCase);
            log.info("In dropdown was selected value " + valueFromCase);
            return true;
        }catch (Exception e) {
            log.error("Catch " + e);
            return false;
        }
    }

    /**
     * Method click on Drop down (DD) and CLICK on option in DD
     * @param dDLocator
     * @param optionLocator
     * @return
     */
    public boolean selectFromDDByOptionLocator(String dDLocator, String optionLocator){
        try{
        	
        	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(ui(dDLocator))));
            WebElement dD = driver.findElement(ui(dDLocator));
            dD.click();
            wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(ui(optionLocator))));
            WebElement option = driver.findElement(ui(optionLocator));
            option.click();
            log.info("Option with Locator " + optionLocator + " was selected");
            return true;
        }catch (Exception e) {
            log.error("Catch " + e);
            return false;
        }
    }

    /**
     * Method checks if element is Displayed and is Enabled
     * @param ElementLocator
     * @return
     */
    public boolean isElementPresent(String ElementLocator){  //if we add 'throws Exception' will be caught exception
        WebElement tempElement;
        try{
            tempElement= driver.findElement(ui(ElementLocator));
            if (tempElement.isDisplayed() && tempElement.isEnabled()){
                log.info("Element " + ElementLocator +"is present");
                return true;
            }else{
                log.info("Element " + ElementLocator + "is not present");
                return false;
            }
        }catch (Exception e) {
            log.error("Catch " + e); // as a separate line will print to log additional info for e into console -  e.printStackTrace();
            return false;
        }
    }

    /**
     * Method to set drop down value using JavaScript
     * @param dropDownJqueryLocator
     * @param dropDownLabel
     * @return
     */
    public boolean selectFromDropDownUsingJS (String dropDownJqueryLocator, String dropDownLabel){    	
    	try{
    		String tempScript = "$('#" + dropDownJqueryLocator + "').val($('#" + dropDownJqueryLocator + "').find('option[label=\"" + dropDownLabel + "\"]').val()).trigger('change')";
    		js.executeScript(tempScript);    		
//    		TODO: insert JQUERY CHECK for drop down value		
    		return true;  		
    	}catch(Exception e){
    		log.error("Value was NOT selected, method selectFromDropDownUsingJS" + e);
    		return false;
    	}
    }
    
    /**
     * Temporary parameter (String radioButtonJqueryLocator) has been removed, until the script will be improved 
     * @param radioButtonValue
     * @return
     */
    public boolean radioButtonSetUsingJS(String radioButtonPartID,String radioButtonValue){
    	try{
    		//selecting value using jquery script
    		js.executeScript("$('#" + radioButtonPartID + radioButtonValue + "').val('" + radioButtonValue + "').trigger('click')");
    		return true;
    		//TODO: review and think how to implement this check with Andriy 
//    		need to understand how to return into Java method boolean or String value from browser JQuery script 
//    		boolean check = false;
//    		String forCheck=js.executeScript(" insert script");
//    		if( check = radioButtonValue){
//    			return true;
//    		} else{
//    			return false;
//    		}
    	}catch(Exception e){
    		log.error("Radio button was not selected, method radioButtonSetUsingJS");
    		return false;
    	}
    }
    
    /**
     * Method to run 
     * @param scriptJS
     * @return
     */
    public boolean runJS(String scriptJS){
    	try{
    		js.executeScript(scriptJS);
    		return true;
    	}catch(Exception e){
    		log.error("");
    		return false;
    	}
    }
    
    

    
    
    
    
    
    
}
