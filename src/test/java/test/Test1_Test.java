package test;

import static libs.ConfigData.ui;
import static org.junit.Assert.*;

//import com.chrome.handler.ChromedriverHandler;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Set;

import org.apache.poi.util.SystemOutLogger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.server.handler.FindElement;
import org.openqa.selenium.remote.server.handler.SendKeys;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import libs.WebElementsOnPage;

public class Test1_Test {
	
	WebElementsOnPage webElementOnPage;
	
	@Test
	public void test() throws MalformedURLException {
		
		//set Application path
		File appDir = new File("src/main/java");
		File app = new File(appDir, "metrics.apk");
		
		//set capabilities
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");		
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "1000"); //timeout after which session is closed
		capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
		
		AndroidDriver driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		
//		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS); //one additional wait
		//for debug, display  current amount of elements with this class name
		
		//thread waiting			
        try {
			Thread.sleep(11000);
			System.out.println("Successfully waited");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		}  
		
        //
		int s = driver.findElements(By.className("android.widget.EditText")).size();
		System.out.println(s);
		
		//click on app menu button and open connections menu
		int appMenuButton = driver.findElements(By.className("android.widget.ImageView")).size();
		System.out.println("Amount of widgets elements available by android.widget.ImageView -  " + appMenuButton);
		driver.findElement(By.className("android.widget.ImageView")).click();
		
        try {
			Thread.sleep(5000);
			System.out.println("Successfully waited");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		}  
		
		//click on Connections
		List <WebElement> appMenu = driver.findElements(By.className("android.widget.TextView"));
		System.out.println("Amount of Web elements in List available by android.widget.TextView -  " + appMenu.size() );
		WebElement connections = appMenu.get(1);
		connections.click();
			
        try {
			Thread.sleep(5000);
			System.out.println("Successfully waited");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		}  
		
		//click on menu button to open menu with "Add new connection" button	
		List <WebElement> connectionsMenu = driver.findElements(By.className("android.widget.ImageButton"));
		System.out.println("Amount of Web elements in List available by android.widget.ImageButton -  " + connectionsMenu.size() );
		WebElement connectionsMenuButton = connectionsMenu.get(0);
		connectionsMenuButton.click();
		
		//click on New Connection List Item
		List <WebElement> createNewConnectionListItem = driver.findElements(By.className("android.widget.TextView"));
		System.out.println("Amount of Web elements in List available by android.widget.TextView -  " + createNewConnectionListItem.size() );
		WebElement newConnectionListItem = createNewConnectionListItem.get(0);
		newConnectionListItem.click();
		
		//enter instance name, instance URL 
		List <WebElement> instanceName = driver.findElements(By.className("android.widget.EditText"));
		System.out.println("Amount of Web elements in List available by android.widget.EditText -  " + instanceName.size() );
		
		WebElement instanceNameInput = instanceName.get(0);
		WebElement instanceURLInput = instanceName.get(1);
		
		instanceNameInput.click();
		instanceNameInput.clear();
		
		String instanceConnectionName = "ADAM_INSTANCE";
		instanceNameInput.clear();
		instanceNameInput.sendKeys(instanceConnectionName);
			
		String instanceURL = "https://adam.metricinsights.com";
		instanceURLInput.click();
		instanceURLInput.clear();
		instanceURLInput.sendKeys(instanceURL);
		
		//click on Save instance
		List <WebElement> saveConnection = driver.findElements(By.className("android.widget.Button"));
		System.out.println("Amount of Web elements in List available by android.widget.Button -  " + saveConnection.size() );
		WebElement saveConnectionButton = saveConnection.get(1);
		saveConnectionButton.click();
		
		//click on instance name TODO: task - choose instance by its name
		List <WebElement> connectionsList = driver.findElements(By.className("android.widget.TextView"));
		System.out.println("Amount of Web elements in List available by android.widget.TextView -  " + connectionsList.size() );
		WebElement adamInstanceConnection = connectionsList.get(11);
		String adamInstanceTextProperty = adamInstanceConnection.getText();
		System.out.println("adamInstanceTextProperty = " + adamInstanceTextProperty);
		
		//click on instance if it match entered name
		if (adamInstanceTextProperty.equals(instanceConnectionName)){
			adamInstanceConnection.click();
			//TODO: return true for future method; TODO: create method tat checks all connections in Webelements list and checks its text while finds match
		} else {
			System.out.println("Instance name to open doesn't match instance name that was entered while creating new connection" );
		}
		
		//wait until instance login page is loaded
        try {
			Thread.sleep(11000);
			System.out.println("Successfully waited");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		}  
		
		//
		List<WebElement> tempWait = driver.findElements(By.className("android.widget.EditText"));
		System.out.println("tempWait " + tempWait.size());
		System.out.println(tempWait);
		wait.until(ExpectedConditions.visibilityOfAllElements(tempWait));
		
		//get available contexts and print its size in array and names
		Set<String> contextNames = driver.getContextHandles();
		System.out.println("Total contexts = " + contextNames.size());

		//for each context in array - print currrent contexts
		for(String context : contextNames) {
			System.out.println(context);
		}
		
		//switch to first available WEBVIEW context and print its name
		for(String context : contextNames) {
			if(context.contains("WEBVIEW")){
				//(This puts Appium session into a mode where all commands are interpreted as being intended for automating the web view)
				driver.context(context);
				System.out.println("Context Name is " + context); // 4.3 Call context() method with the id of the context you want to access and change it to WEBVIEW_1
				break;
			}	
		}		
				
//		WebElement login = driver.findElement(By.xpath(".//*[@id='login']"));
//		login.click();
//		login.sendKeys("alexl");
		
		//enter login credentials to application
		driver.findElement(By.xpath(".//input[@id='login']")).click();
		driver.findElement(By.xpath(".//input[@id='login']")).sendKeys("alexl");
		
		//check if code get in here		
		System.out.println("did code get there?))");
		
//		WebElement password = driver.findElement(By.xpath(".//*[@id='password']"));
//		password.click();
//		password.sendKeys("1");	
//		driver.findElement(By.xpath(".//*[@id='loginButton']")).click();
		
		//enter password and click on Login button
		driver.findElement(By.xpath(".//input[@id='password']")).click();
		driver.findElement(By.xpath(".//input[@id='password']")).sendKeys("1");
		driver.findElement(By.xpath(".//button[@id='loginButton']")).click();
		
		System.out.println("looks like yes...");		
			
		//wait while Homepage is loaded
        try {
			Thread.sleep(30000);
			System.out.println("Successfully waited");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
		
        // get array of available contexts and print its size and names
		Set<String> contextNames2 = driver.getContextHandles();
		System.out.println("Total contexts = " + contextNames2.size());
		
		for(String context : contextNames2) {
			System.out.println(context);
		}
		
		//select a Category	by text of Name	
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(".//select[@id='category']"))));
        WebElement parentDropDownElement;
        parentDropDownElement = driver.findElement(By.xpath(".//select[@id='category']"));
        Select selectFromParentDropDownElement;
        selectFromParentDropDownElement = new Select(parentDropDownElement);
        selectFromParentDropDownElement.selectByVisibleText("Smoke Mobile Automated");
		
		//wait until category values are loaded
        try {
			Thread.sleep(15000);
			System.out.println("Successfully waited");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		}	
		
        // get array of available contexts and print its size and names
		Set<String> contextNames3 = driver.getContextHandles();
		System.out.println("Total contexts = " + contextNames3.size());

		for(String context : contextNames3) {
			System.out.println(context);
		}
		
		// find Metric by text of Name value and click 
		try {
			WebElement metricTile = driver.findElement(By.xpath(".//a[text()='Smoke  Mobile Automated Metric for wine']"));
			wait.until(ExpectedConditions.visibilityOf(metricTile)).click();;
			System.out.println("Metric link Was clicked clicked");
		} catch(Exception e) {
			System.out.println("Error, wasn't clicked");
		}
		
		//GET METRIC PAGE URL DUE TO A BUG
		String currentMetricPage = driver.getCurrentUrl();
		
		//wait until Metric viewer is loaded
        try {
			Thread.sleep(30000);
			System.out.println("Successfully waited");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
		
		//adding Annotation to Metric
        wait.until(ExpectedConditions.visibilityOf( driver.findElement( By.xpath(".//button[@id='addFutureAnnotation']") ) ) ).click();
        
        try {
			Thread.sleep(7000);
			System.out.println("Successfully waited until annotation popup is loaded");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
        
        
        wait.until(ExpectedConditions.visibilityOf( driver.findElement( By.xpath(".//textarea[@id='ann_text']") ) ) ).click();
        WebElement annotationTextArea = driver.findElement( By.xpath(".//textarea[@id='ann_text']") );
        String annotationText = "Annotation to Metric for 2016-02-20";
        
        js.executeScript("$(\"#annotation_date_on\").val('2016-02-20')");
        // $("#annotation_date_on").val('2016-02-20')
        
        annotationTextArea.clear();
        annotationTextArea.sendKeys(annotationText);
        
        //click on Save Annotation button
        wait.until(ExpectedConditions.visibilityOf( driver.findElement( By.xpath(".//button[@id='save']") ) )).click();
        
        //wait until annotation is saved
        try {
			Thread.sleep(10000);
			System.out.println("Successfully waited until annotation is saved");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
        
        // reload page due to bug
        driver.get(currentMetricPage);
        
        //wait until page is reloaded due to bug
        try {
			Thread.sleep(10000);
			System.out.println("Successfully waited until page is reloaded");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
        
        // check if annotation appear in Collaborative section
//        List <WebElement> annotationsList = driver.findElements(By.className("media ann_element_item wall_annotation_node"));
//        for (WebElement searchAnnotation : annotationsList){
//        	if (searchAnnotation.getText().equals( annotationText ) ){
//        		System.out.println( "Annotation text matches"  );
//        	}
//        }
        
        //TODO: need to scroll page to collaborative wall!                
//        webElementOnPage.isElementPresent(".//div[@class='ann_element_item_cont markdown']/p[text()='Annotation to Metric for 2016-02-20']"); //TODO: create separate file for al data and separate variable for Annotation text       	
        
        
        
        try {
			Thread.sleep(10000);
			System.out.println("Successfully waited until Homepage is loaded");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		}  
        
        //proceed to Homepage   - this code didn't work, replaced with next code:
//        wait.until( ExpectedConditions.visibilityOf(driver.findElement( By.className("fa fa-home") ) ) ).click();
        driver.get(instanceURL);
        
        
        //wait until Homepage is loaded
        try {
			Thread.sleep(15000);
			System.out.println("Successfully waited until Homepage is loaded");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
                
		// find Report by text of Name value and click 
		try {
			WebElement reportTile = driver.findElement(By.xpath(".//a[text()='Smoke Mobile Automated Report']"));
			wait.until(ExpectedConditions.visibilityOf(reportTile)).click();;
			System.out.println("Report link Was clicked");
		} catch(Exception e) {
			System.out.println("Error, wasn't clicked");
		}
        
        //wait until Report viewer is loaded
        try {
			Thread.sleep(30000);
			System.out.println("Successfully waited until Report viewer is loaded");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
		
        // get current url to come back to it
        String reportURL = driver.getCurrentUrl();
        
		//open Report Chart Zoom view
        js.executeScript("$('#chart_0').trigger(\"onclick\")");
        
        try {
			Thread.sleep(7000);
			System.out.println("Successfully waited until opening Report Chart Zoom view");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
                
        //open annotation popup on Zoom view
        js.executeScript("$('#map_area_0_0').trigger('click')");
        
        //wait until popup appear
        try {
			Thread.sleep(10000);
			System.out.println("Successfully waited until popup appear");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
        
        //click on nannotation_add
        WebElement addAnnotationPopupLink = driver.findElement(By.xpath(".//div[@id='map_div_click_0_0']//a[@class='nannotation_add']"));
        addAnnotationPopupLink.click();
        
        //wait
        try {
			Thread.sleep(10000);
			System.out.println("Successfully waited until element is loaded");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
        
        //adding Annotation to Report		
        WebElement annotationTextAreaInAddNewAnnotationPopup = driver.findElement(By.xpath(".//textarea[@id='annotation_text']"));
        String reportAnnotationText = "Annotation to Report for 2016-02-20";
        annotationTextAreaInAddNewAnnotationPopup.click();
        annotationTextAreaInAddNewAnnotationPopup.sendKeys(reportAnnotationText);
        
        //wait while adding Annotation to Report
        try {
			Thread.sleep(5000);
			System.out.println("Successfully waited while adding Annotation to Report");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
        
        // click on Save button in Add new annotation
        driver.findElement(By.xpath("//div[@id='div_long_buttons']/button[text()='Save']"));
        
        //wait while saving anotation
        try {
			Thread.sleep(5000);
			System.out.println("Successfully waited while saving anotation");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
        
        //get back to Report viewer page
        driver.get(reportURL);
            
        try {
			Thread.sleep(10000);
			System.out.println("Successfully waited while get back to Report viewer page");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
        
        
        //check if annotation to Report is present TODO: edit this check
//        webElementOnPage.isElementPresent(".//div[@class='ann_element_item_cont markdown']/p[text()='Annotation to Report for 2016-02-20']");
              
        //expand "More info" collaborative section
        js.executeScript("$('#carouselViewer > div.carousel-inner > div.view-content-box.item.active.place-for-ind > div.switch-visible-annot-wrap > span').trigger('click')");
        
        try {
			Thread.sleep(10000);
			System.out.println("Successfully waited while expand \"More info\" collaborative section");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
        
		//check if Annotation appear in collaborative section        
//      webElementOnPage.isElementPresent(".//div[@class='ann_element_item_cont markdown']/p[text()='Annotation to Report for 2016-02-20']");
        
        driver.get(instanceURL);
        
        try {
			Thread.sleep(10000);
			System.out.println("Successfully waited while loading Homepage");
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("Thread sleep is failed" + e);
		} 
        
        //Quit driver
		try{
			driver.quit();
			System.out.println("driver.quit executed" );
		} catch(Exception e){
			e.printStackTrace();
			System.out.println("driver didn\'t quit");
		}
		
		try {
			Runtime.getRuntime().exec("adb -s emulator-5554 emu kill");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
//		try {
//			Runtime.getRuntime().exec("tskill emulator-arm");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			System.out.println("driver didn\'t quit");
//		}
		
		//TODO: AFTER TEST - DELETE ALL CREATED ANNOTATIONS
		
        
//		try {
//		for (String contextName : contextNames) {
//			System.out.println(contextNames);
//			driver.context("WEBVIEW_com.metricinsights.viewer");
//		}
//	} catch (Exception e) {
//		System.out.println("Error " + e);
//		e.printStackTrace();
//	}
		
      //span[@class="custom_time"]
      //textarea[@id="ann_text"]
      //button[@id="save"]
      //input[@id='annotation_date_on']
		
        //set calendar time 
       // $("#annotation_date_on").val('2016-02-20')

		//creating list that consist of elements with given class name, filling inputs
//		List<WebElement>login=driver.findElements(By.className("android.widget.EditText"));
//		login.get(0).sendKeys("alexl");
//		login.get(1).sendKeys("1");
//		//debug info and click on Login button
//		int b = driver.findElements(By.className("android.widget.Button")).size();
//		System.out.println(b);
//		List<WebElement>loginButton=driver.findElements(By.className("android.widget.Button"));
//		loginButton.get(0).click();//clicking login button, it's under 0 index in loginButton list
				
		//pseudocode for usage content-description
		
		//login_image = driver.findElementByXPath("//android.widget.ImageView[@content-desc='Login']"); 
		// Gets you the WebElement
//		driver.context("WEBVIEW");
		
//		WebElement search = driver.findElement(By.xpath(".//input[@id='search']"));
//		wait.until(ExpectedConditions.visibilityOf(search));
//		search.click();
//		search.sendKeys("smoke");
//		search.sendKeys(Keys.ENTER);
		
	
//		int e = driver.findElements(By.className("android.widget.EditText")).size();
//		System.out.println(e);
//		
//		//TODO: insert WAIT !!! 
//		List<WebElement>searchInput=driver.findElements(By.className("android.widget.EditText"));
//		System.out.println(searchInput);
//		searchInput.get(0).click();
//		searchInput.get(0).sendKeys("smoke");
//		searchInput.get(0).sendKeys(Keys.ENTER);
//	
//		driver.context("WEBVIEW_com.metricinsights.viewer");
//		WebElement tempMobile;
//		driver.findElement(By.xpath(".//*[@id='search']")).click();
				
//		tempMobile.sendKeys("smoke").sendKeys(Keys.ENTER);		
		
//		Set<String>contextNames = driver.getContextHandles();
//		for (String contextName :contextNames) {
//			      System.out.println(contextNames);
//			  }
//		
//		driver.context(contextNames.toArray()[1]); // set context to WEBVIEW_1
//		//do some web testing
//		String myText = driver.findElement(By.xpath(".//*[@id='login']")).click();
//
//		driver.context("NATIVE_APP");

		// do more native testing if we want		
	
	}

}